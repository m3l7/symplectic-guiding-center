# Symplectic Guiding Center Integrator #

A symplectic algorithm for the integration for the non canonical guiding center equations of motion and possibly other problems.
Work done as part of the master degree thesis.

## Build

make sure to have gsl installed in your system, then run

      ./build
      ./a.out